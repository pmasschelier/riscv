#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define OPCODE(code) (code & 0x7f)
#define FUNC3(code) ((code >> 12) & 7)
#define FUNC7(code) ((code >> 25) & 0x7f)
#define RS1(code) ((code >> 15) & 0x1f)
#define RS2(code) ((code >> 20) & 0x1f)
#define RD(code) ((code >> 7) & 0x1f)

const uint32_t code[22] = {
	0x00050893,
	0x00068513,
	0x04088063,
	0x04058263,
	0x04060063,
	0x04d05063,
	0x00088793,
	0x00269713,
	0x00e888b3,
	0x0007a703,
	0x0005a803,
	0x01070733,
	0x00e62023,
	0x00478793,
	0x00458593,
	0x00460613,
	0xff1792e3,
	0x00008067,
	0xfff00513,
	0x00008067,
	0xfff00513,
	0x00008067
};

typedef enum {
	R, // Instructions operating on Registers (add, or, sll, slt, div, ...)
	I, // Instructions taking an immediate constant (addi, andi, srai, sltiu, lw, jalr, ...)
	S, // Stores/cond. branches with an immediate constant (sw, ...)
	SB, // idem (beq, ...)
	U, // Instructions/jumps taking a large immediate constant (lui, ...)
	UJ // idem (jal, ...)
} FMT;

int32_t imm_fmti(uint32_t code) {
	uint32_t ret = (code >> 20); // bits [31: 20]
	return (int32_t)((code >> 31 & 1) ? (ret | (0xFFFFFFFF << 12)) : ret);
}

int32_t imm_fmts(uint32_t code) {
	uint32_t ret = (code >> 20) | ((code >> 7) & 0x1f);
	return (int32_t)((code >> 31 & 1) ? (ret | (0xFFFFFFFF << 12)) : ret);
}

int32_t imm_fmtsb(uint32_t code) {
	uint32_t ret = (((code >> 20) & 0x7e0) | ((code >> 7) & 0x1e) | ((code << 4) & (1 << 11)) | ((code >> 19) & (1 << 12)));
	return (int32_t)((code >> 31 & 1) ? (ret | (0xFFFFFFFF << 12)) : ret);
}

int32_t imm_fmtu(uint32_t code) {
	uint32_t ret = (code >> 12); // bits [31: 12]
	return (int32_t)((code >> 31 & 1) ? (ret | (0xFFFFFFFF << 20)) : ret);
}

int32_t imm_fmtuj(uint32_t code) {
	uint32_t ret = (code & (0xFFFFFFFF >> 12));
	return (int32_t)ret;
}

const char* regs[] = {
	"zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0/fp", "s1",
	"a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11",
	"t3", "t4", "t5", "t6","ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7", 
	"fs0", "fs1", "fa0", "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7", 
	"fs2", "fs3", "fs4", "fs5", "fs6", "fs7", "fs8", "fs9", "fs10", "fs11", 
	"ft8", "ft9", "ft10", "ft11"
};

const char* regnum[] = {
	"x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16",
	"x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31",
	"f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", "f14", "f15", "f16",
	"f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30", "f31"
};

/*char* regnum(unsigned int num, char buf[]) {
	buf[0] = (num <= 31) ? 'x' : 'f';
	snprintf(buf+1, 3, "%d", (num <= 31) ? num : num - 32);
	return buf;
}*/

FMT mnemonic(uint32_t code, char** mnemonic) {
	const uint32_t opcode = OPCODE(code);
	const uint32_t func3 = FUNC3(code);
	const uint32_t func7 = FUNC7(code);

	*mnemonic = NULL;
	FMT fmt;

	switch (opcode)
	{
	case 0x3:
		switch (func3)
		{
		case 0x0: *mnemonic = "lb"; break;
		case 0x1: *mnemonic = "lh"; break;
		case 0x2: *mnemonic = "lw"; break;
		case 0x4: *mnemonic = "lbu"; break;
		case 0x5: *mnemonic = "lhu"; break;
		default: break;
		}
		fmt = I;
		break;
	case 0xf:
		switch (func3)
		{
		case 0x0: *mnemonic = "fence"; break;
		case 0x1: *mnemonic = "fence.i"; break;
		default: break;
		}
		fmt = I;
		break;
	case 0x13:
		switch (func3)
		{
		case 0x0: *mnemonic = "addi"; break;
		case 0x1: *mnemonic = "slli"; break;
		case 0x2: *mnemonic = "slti"; break;
		case 0x4: *mnemonic = "sltiu"; break;
		case 0x5: *mnemonic = "xori"; break;
		case 0x6:
			switch (func7)
			{
			case 0x0: *mnemonic = "srli"; break;
			case 0x20: *mnemonic = "srai"; break;
			default: break;
			}
		default: break;
		}
		fmt = I;
		break;
	case 0x17: *mnemonic = "auipc"; fmt = U; break;
	case 0x23:
		switch (func3)
		{
		case 0x0: *mnemonic = "sb"; break;
		case 0x1: *mnemonic = "sh"; break;
		case 0x2: *mnemonic = "sw"; break;
		default: break;
		}
		fmt = S;
		break;
	case 0x33:
		switch (func3)
		{
		case 0x0:
			switch (func7)
			{
			case 0x0: *mnemonic = "add"; break;
			case 0x20: *mnemonic = "sub"; break;
			default: break;
			}
			break;
		case 0x1: *mnemonic = "sll"; break;
		case 0x2: *mnemonic = "slt"; break;
		case 0x4: *mnemonic = "sltu"; break;
		case 0x5: *mnemonic = "xor"; break;
		case 0x6:
			switch (func7)
			{
			case 0x0: *mnemonic = "srl"; break;
			case 0x20: *mnemonic = "sra"; break;
			default: break;
			}
			break;
		case 0x7: *mnemonic = "or"; break;
		case 0x8: *mnemonic = "and"; break;
		default: break;
		}
		fmt = R;
		break;
	case 0x37: *mnemonic = "lui"; fmt = U; break;
	case 0x63:
		switch (func3)
		{
		case 0x0: *mnemonic = "beq"; break;
		case 0x1: *mnemonic = "bne"; break;
		case 0x4: *mnemonic = "blt"; break;
		case 0x5: *mnemonic = "bge"; break;
		case 0x6: *mnemonic = "bltu"; break;
		case 0x7: *mnemonic = "bgeu"; break;
		default: break;
		}
		fmt = SB;
		break;
	case 0x67: *mnemonic = "jalr"; fmt = I; break;
	case 0x6f: *mnemonic = "jal"; fmt = UJ; break;
	case 0x73:
		switch (func3)
		{
		case 0x0:
			switch (func7)
			{
			case 0x0: *mnemonic = "ecall"; break;
			case 0x1: *mnemonic = "ebreak"; break;
			default: break;
			}
			break;
		case 0x1: *mnemonic = "CSRRW"; break;
		case 0x2: *mnemonic = "CSRRS"; break;
		case 0x3: *mnemonic = "CSRRC"; break;
		case 0x5: *mnemonic = "CSRRWI"; break;
		case 0x6: *mnemonic = "CSRRSI"; break;
		case 0x7: *mnemonic = "CSRRCI"; break;
		default: break;
		}
		fmt = I;
		break;
	
	default:
		break;
	}
	if(*mnemonic == NULL) {
		const size_t size = 23;
		*mnemonic = malloc(size * sizeof(char));
		fprintf(stderr, "UNKNOWN : %x / %x / %x", opcode, func3, func7);
		return -1;
	}
	return fmt;
}


int main() {
	puts("|\tLine\t|\topcode\t\t|\trd\t|\trs1\t|\trs2\t|\timm");
	puts("|\t---\t|\t---\t\t|\t---\t|\t---\t|\t---\t|\t---");
	for(int i=0; i < 22; i++) {
		const char *reg1 = "", *reg2 = "", *reg3 = "";
		const char *num1 = "", *num2 = "", *num3 = "";
		char* m = NULL;
		char imm[11] = "";

		FMT fmt = mnemonic(code[i], &m);

		if(fmt == R || fmt == I || fmt == U || fmt == UJ) {
			num1 = regnum[RD(code[i])];
			reg1 = regs[RD(code[i])];
		}
		if(fmt == R || fmt == I || fmt == S || fmt == SB) {
			num2 = regnum[RS1(code[i])];
			reg2 = regs[RS1(code[i])];
		}
		if(fmt == R || fmt == I || fmt == SB) {
			num3 = regnum[RS2(code[i])];
			reg3 = regs[RS2(code[i])];
		}
		switch (fmt)
		{
		case I: snprintf(imm, 11, "%d", imm_fmti(code[i])); break;
		case S: snprintf(imm, 11, "%d", imm_fmts(code[i])); break;
		case SB: snprintf(imm, 11, "%d", imm_fmtsb(code[i])); break;
		case U: snprintf(imm, 11, "%d", imm_fmtu(code[i])); break;
		case UJ: snprintf(imm, 11, "%d", imm_fmtuj(code[i])); break;
		default: break;
		}
		
		printf("|\t%d\t|\t%s\t\t|\t%s / %s\t|\t%s / %s\t|\t%s / %s\t|\t%s\n", i, m, num1, reg1, num2, reg2, num3, reg3, imm);
	}

	return 0;
}