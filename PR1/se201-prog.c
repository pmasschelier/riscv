int sum(unsigned* a, unsigned* b, unsigned* c, int n) {
	if(!a || !b || !c)
		return -1;
	if(n <= 0)
		return n;
	
	unsigned* end = a + n;
	while(a != end) {
		*c = *a + *b;
		a++;
		b++;
		c++;
	}
	return n;
}