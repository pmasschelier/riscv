<div style="text-align: justify"> 

# Project PR1 : RISCV

DUFFO Yann, IHADADENE Cilia, MASCHELLIER Paul-Marie

## Introduction 

In this project, the goal is to become familiar with assembly code (mostly only RISC-V). To do this, we will start by studying 32-bit assembly instructions and decode them as we did in class. 
The goal is to understand how this code works in depth. To do this, we will often draw parallels with C code which is often more readable. 
Then to finish, we will start the creation of our own processor with its own 16bits instruction set. To succeed in making this processor, we will need to master the notions of registers, instructions or pipeline.
A lot of new knowledge has to be assimilated and will be for the most part summarized in this document.


## 1. RISC-V Instruction Set

To begin with, we need to decode an assembler program. To do this, we have written a C code named "code.c" which analyzes and decomposes each line and makes it easy to understand the basic assembly instructions.

Below is the assembly code to be translated:

```
0: 00050893
4: 00068513
8: 04088063
c: 04058263
10: 04060063
14: 04d05063
18: 00088793
1c: 00269713
20: 00e888b3
24: 0007a703
28: 0005a803
2c: 01070733
30: 00e62023
34: 00478793
38: 00458593
3c: 00460613
40: ff1792e3
44: 00008067
48: fff00513
4c: 00008067
50: fff00513
54: 00008067
```

By compiling `gcc -Wall -Wextra -pedantic -std=c11 -fno-common -fno-builtin -o code code.c` and running we obtain :

|	Line	|	opcode		|	rd	|	rs1	|	rs2	|	imm
|	---	|	---		|	---	|	---	|	---	|	---
|	0	|	addi		|	x17 / a7	|	x10 / a0	|	x0 / zero	|	0
|	1	|	addi		|	x10 / a0	|	x13 / a3	|	x0 / zero	|	0
|	2	|	beq		|	 / 	|	x17 / a7	|	x0 / zero	|	64
|	3	|	beq		|	 / 	|	x11 / a1	|	x0 / zero	|	68
|	4	|	beq		|	 / 	|	x12 / a2	|	x0 / zero	|	64
|	5	|	bge		|	 / 	|	x0 / zero	|	x13 / a3	|	64
|	6	|	addi		|	x15 / a5	|	x17 / a7	|	x0 / zero	|	0
|	7	|	slli		|	x14 / a4	|	x13 / a3	|	x2 / sp	|	2some discription
|	19	|	jalr		|	x0 / zero	|	x1 / ra	|	x0 / zero	|	0
|	20	|	addi		|	x10 / a0	|	x0 / zero	|	x31 / t6	|	-1
|	21	|	jalr		|	x0 / zero	|	x1 / ra	|	x0 / zero	|	0

By quickly analyzing these commands, we can write the pseudo-code below. It will allow us to understand much more easily what the program does and then to consider translating it into C.

This gives in pseudo-code :
```
a7 = a0 + 0
a0 = a3 + 0
if(a7 == 0) PC = PC + 16
if(a1 == 0) PC = PC + 17
if(a2 == 0) PC = PC + 16
if(0 >= a3) PC = PC + 16
a5 = a7 + 0
a4 = a3 << 2
a7 = a7 + a4
a4 = MEM[a5 + 0]
a6 = MEM[a1 + 0]
a4 = a4 + a6
MEM[a2 + 0] = a4
a5 = a5 + 4
a1 = a1 + 4
a2 = a2 + 4
if(a5 != a7) PC = PC - 7
ret
a0 = - 1
ret
a0 = - 1
ret
```
To quickly go back to the assembly command analysis C code we wrote, here is a brief explanation of what it is able to do.

code.c includes :
- the bytes array containing the code
- the macros **OPCODE**, **FUNC3**, **FUNC7**, **RS1**, **RS2**, **RD** extracting the different parts of the instruction
- the functions **imm_fmti**, **imm_fmts**, **imm_fmtsb**, **imm_fmtu**, **imm_fmtuj** extracting the immediate for the corresponding instruction format
- the **regs** and **regnum** arrays associating the register number with its names
- the **mnemonic** function which returns the format of the instruction and the mnemonic associated by its parameters (it's a big switch case).

You will have understood that we are very proud of this code on which we worked a lot. It is versatile and decodes with great precision the assembly commands written in RISC-V.

### Explanation of branch delay slot :

According to wikipedia, the delay slot is an instruction slot being executed without the effects of a preceding instruction. This happens often on the MIPS architecture with jump and branch instructions, the instruction after the jump or branch instruction is executed before the jump or branch.

The location of the delay slot instruction when a branch instruction is involved is called branch delay slot. The processor will execute instructions following a branch instruction before the branch takes effect.

Assemblers reorder instructions to move some instructions immediately after branch instruction, such that the moved instruction will always be executed, regardless whether branch is taken or not, without leaving the system in an inconsistent state. A simple design would insert stalls into the pipeline after a branch instruction until the new branch target address is computed and loaded into the program counter. 

The advantage of a branch delay slot is to gain time when running the pipeline. The disadvantage is that the hardware is directly related to architecture, so when we upgrade the hardware, the binary code becomes no longer compatible with the new architecture.

example : 
```
add r1, r2, r3;
b anywhere;
anywhere: sub
```

### Conditional branches

According to the pseudo code given above, here is a list of the conditional branches in the function and their target addresses:

```
beq x0, x17, x0, 64    ---> branch to line 18
beq x4, x11, x0, 68    ---> branch to line 20
beq x0, x12, x0, 64    ---> branch to line 20
bge x0, x0, x13, 64    ---> branch to line 21
bne x5, x15, x17, -28  ---> branch to line 9
```

We notice that there are several branches but we will notice later that the conditions are not always verified at the first pass.

### Function behavior

Below you can find the C code that corresponds to the assembler program that was given to us at the beginning of the topic. 
Explanations on how it works and what it does are available below.

```
f(a0, a1, a2, a3) {
	a7 = a0
	a0 = a3
	if(a7 == 0 || a1 == 0 || a2 == 0)
		return -1
	if(a3 <= 0)
		return a0
	a5 = a7
	a4 = a3 * 4
	a7 += a4
	do {
		a4 = MEM[a5]
		a6 = MEM[a1]
		a4 += a6
		MEM[a2] = a4
		a5 += 4
		a1 += 4
		a2 += 4
	} while(a5 != a7);
	return a0;
}
```

The function takes in arguments the adresses a0, a1, a2 and the value a3.
If one of the adress is 0x0 the function returns -1.
if a3 is negative the function returns a3 indeed a3 is the number of bytes to sum.
Then the loop will read a3*4 bytes starting from the adresses a0 and a1, sum each couple of 4 bytes read.
and store the result in a table beginning at adress a2.
Eventually the function return the number of bytes summed and copied.


## 2. RISC-V Tool Chain

We wrote the C code that corresponds to the assembler program we were given in the previous part. 
To summarize, this code reads 2 arrays and adds them element by element then stores them in a new array.

```c
uint32_t* sum(uint32_t* a, uint32_t* b, uint32_t* c, size_t n) {
	if(!a || !b || !c)
		return -1;
	if(n <= 0)
		return n;
	
	uint32_t* end = a + n;
	while(a != end) {
		*c = *a + *b;
		a++
		b++;
		c++;
	}
	return n;
}
```

### Disassembling (compilation with -O0)

We then disassembled the program once we had compiled it with ggc. The disassembling was done with the objdump tool as requested in the statement.

```asm

se201-prog.o:     format de fichier elf32-littleriscv


Déassemblage de la section .text :

00000000 <sum>:
   0:	fd010113          	addi	sp,sp,-48
   4:	02812623          	sw	s0,44(sp)
   8:	03010413          	addi	s0,sp,48
   c:	fca42e23          	sw	a0,-36(s0)
  10:	fcb42c23          	sw	a1,-40(s0)
  14:	fcc42a23          	sw	a2,-44(s0)
  18:	fcd42823          	sw	a3,-48(s0)
  1c:	fdc42783          	lw	a5,-36(s0)
  20:	00078a63          	beqz	a5,34 <.L2>
  24:	fd842783          	lw	a5,-40(s0)
  28:	00078663          	beqz	a5,34 <.L2>
  2c:	fd442783          	lw	a5,-44(s0)
  30:	00079663          	bnez	a5,3c <.L3>

00000034 <.L2>:
  34:	fff00793          	li	a5,-1
  38:	07c0006f          	j	b4 <.L4>

0000003c <.L3>:
  3c:	fd042783          	lw	a5,-48(s0)
  40:	00f04663          	bgtz	a5,4c <.L5>
  44:	fd042783          	lw	a5,-48(s0)
  48:	06c0006f          	j	b4 <.L4>

0000004c <.L5>:
  4c:	fd042783          	lw	a5,-48(s0)
  50:	00279793          	slli	a5,a5,0x2
  54:	fdc42703          	lw	a4,-36(s0)
  58:	00f707b3          	add	a5,a4,a5
  5c:	fef42623          	sw	a5,-20(s0)
  60:	0440006f          	j	a4 <.L6>

00000064 <.L7>:
  64:	fdc42783          	lw	a5,-36(s0)
  68:	0007a703          	lw	a4,0(a5)
  6c:	fd842783          	lw	a5,-40(s0)
  70:	0007a783          	lw	a5,0(a5)
  74:	00f70733          	add	a4,a4,a5
  78:	fd442783          	lw	a5,-44(s0)
  7c:	00e7a023          	sw	a4,0(a5)
  80:	fdc42783          	lw	a5,-36(s0)
  84:	00478793          	addi	a5,a5,4
  88:	fcf42e23          	sw	a5,-36(s0)
  8c:	fd842783          	lw	a5,-40(s0)
  90:	00478793          	addi	a5,a5,4
  94:	fcf42c23          	sw	a5,-40(s0)
  98:	fd442783          	lw	a5,-44(s0)
  9c:	00478793          	addi	a5,a5,4
  a0:	fcf42a23          	sw	a5,-44(s0)

000000a4 <.L6>:
  a4:	fdc42703          	lw	a4,-36(s0)
  a8:	fec42783          	lw	a5,-20(s0)
  ac:	faf71ce3          	bne	a4,a5,64 <.L7>
  b0:	fd042783          	lw	a5,-48(s0)

000000b4 <.L4>:
  b4:	00078513          	mv	a0,a5
  b8:	02c12403          	lw	s0,44(sp)
  bc:	03010113          	addi	sp,sp,48
  c0:	00008067          	ret
```

The code is very different from the one we studied in the first part, let's see why :
- The code was compiled with the -O0 option which is known to increase considerably the size of the code discarding every optimization that can be done.
- The 7 first line handles the **stack frame** and copy the parameters (a0, a1, a2, a3) in the stack which looks this way after (0 is the top of the stack at the beginning of the function) :

| Adress 	| Reg. saved	| Reg. pointing here	|
| ---		| ---			| ---	|
| 0 		| 				| fp	|
| -4		| 	fp			|		|
| ...		| 	...			| ...	|
| -36		| 	a0			|		|
| -40		| 	a1			|		|
| -44		| 	a2			|		|
| -48		| 	a3			| sp	|

- Symmetrically, the symbol .L4 corresponds to the **epilogue of the function**: the stack frame is restored and a5 is returned.
- The instructions from 0x1c to 0x38 corresponds to the line `if(!a || !b || !c) return -1;`
- .L3 corresponds to the line `if(n <= 0) return n;`
- .L5 corresponds to the line `uint32_t* end = a + n;`
- .L6 corresponds to the while condition
- .L7 corresponds to the body of the while loop

*The main reason why the code is so much larger than the original one is that before every operation on a register it is loaded from memory and after every operation it is saved back into the stack.*
*Another reason is the prologue and the epilogue of the function due to the C language.*

### Other compilation options

#### With -O1

It should be noted that depending on the optimization requested at compile time, the assembly code will not be quite the same.

```asm
se201-prog.o:     format de fichier elf32-littleriscv


Déassemblage de la section .text :

00000000 <sum>:
   0:	00050793          	mv	a5,a0
   4:	00068513          	mv	a0,a3

00000008 <.LVL1>:
   8:	04078063          	beqz	a5,48 <.L4>
   c:	04058263          	beqz	a1,50 <.L5>
  10:	04060463          	beqz	a2,58 <.L6>
  14:	04d05463          	blez	a3,5c <.L2>
  18:	00269893          	slli	a7,a3,0x2
  1c:	011788b3          	add	a7,a5,a7

00000020 <.LVL2>:
  20:	03178e63          	beq	a5,a7,5c <.L2>

00000024 <.L3>:
  24:	0007a703          	lw	a4,0(a5)
  28:	0005a803          	lw	a6,0(a1)
  2c:	01070733          	add	a4,a4,a6
  30:	00e62023          	sw	a4,0(a2)
  34:	00478793          	addi	a5,a5,4
  38:	00458593          	addi	a1,a1,4

0000003c <.LVL4>:
  3c:	00460613          	addi	a2,a2,4

00000040 <.LVL5>:
  40:	fef892e3          	bne	a7,a5,24 <.L3>
  44:	00008067          	ret

00000048 <.L4>:
  48:	fff00513          	li	a0,-1
  4c:	00008067          	ret

00000050 <.L5>:
  50:	fff00513          	li	a0,-1

00000054 <.LVL8>:
  54:	00008067          	ret

00000058 <.L6>:
  58:	fff00513          	li	a0,-1

0000005c <.L2>:
  5c:	00008067          	ret
```

We immediatly notice that the prologue and epilogue parts were dismissed and the registers are no longer saved to the stack. We know get a code of only 24 instructions which is quite close to the original code as much by its size as by its shape.

#### With -02, -03 or -Os

We know get an even smaller code (20 instructions) by using registers and branch instructions in a smart way to reduce the number of mv and ret instructions.

## 3. RISC-V Architecture

### 3.1 Program Flow

In this question, we have to provide a full list of instructions until the function terminates by executing a ret instruction. 

You will find below a table showing each instruction with its explanation, we also explained the three types of data hazard that occures and their type.

In the table below we have the evolution of the registers according for each instruction. We also added an explanation in humain readable language.

| PC 	|  instruction		| a0   | a1   | a2   | a3   | a4   | a5   | a6   | a7   | explanation
| ---	|	---				|---   |---   |---   |---   |---   |---   |---   | ---  | ---
| 0x0	| mv a7, a0	    	|0x200 |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | copy value of a0 in a7
| 0x4	| mv a0, a3			|0x2   |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | copy value of a3 in a0
| 0x8	| beq a7, x0, 64	|0x2   |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | DATA HAZARD : a7 is forwarded. a7 is not equal to 0 so nothing is done
| 0xc	| beq a1, x0, 68	|0x2   |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | a1 is not equal to 0 so nothing is done
| 0x10	| beq a2, x0, 64	|0x2   |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | a2 is not equal to 0 so nothing is done
| 0x14	| bge x0, a3, 64	|0x2   |0x200 |0x200 |0x2   |0x0   |0x0   | 0x0  |0x200 | 0 lower than 0x2 so nothing is done
| 0x18	| mv a5, a7	    	|0x2   |0x200 |0x200 |0x2   |0x0   |0x200 | 0x0  |0x200 | copy value of a7 in a5
| 0x1c	| slli a4, a3, 2    |0x2   |0x200 |0x200 |0x2   |0x8   |0x200 | 0x0  |0x200 | shift 2 times left a3 and store it in a4
| 0x20	| add a7, a7, a4	|0x2   |0x200 |0x200 |0x2   |0x8   |0x200 | 0x0  |0x208 | DATA HAZARD : a4 is forwarded. add a7 and a4 and store it in a7
| 0x24	| lw a4, a5, 0 		|0x2   |0x200 |0x200 |0x2   |0x61  |0x200 | 0x0  |0x208 | DATA HAZARD : stall by wainting a4. copy value pointed by a5 in a4
| 0x28	| lw a6, a1, 0 		|0x2   |0x200 |0x200 |0x2   |0x61  |0x200 | 0x61 |0x208 | copy value pointed by a1 in a6
| 0x2c	| add a4, a4, a6 	|0x2   |0x200 |0x200 |0x2   |0xc2  |0x200 | 0x61 |0x208 | DATA HAZARD : stall by wainting a6. add a4 and a6 and store it in a4
| 0x30	| sw a4, a2	    	|0x2   |0x200 |0x200 |0x2   |0xc2  |0x200 | 0x61 |0x208 | DATA HAZARD : a4 is forwarded. store a4 in pointed value of a2 (mem[0x200]=0xc2)
| 0x34	| addi a5, a5, 4	|0x2   |0x200 |0x200 |0x2   |0xc2  |0x204 | 0x61 |0x208 | add 4 to a5
| 0x38	| addi a1, a1, 4	|0x2   |0x204 |0x200 |0x2   |0xc2  |0x204 | 0x61 |0x208 | add 4 to a1
| 0x3c	| addi a2, a2, 4	|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | add 4 to a2
| 0x40	| bne a5, a7, -28	|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | a5 not equal to a7 so PC = PC - 7
| 0x44	| jarl ra			|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | flushed
| 0x48	| addi a0, -1		|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | flushed
| 0x4c	| jarl ra			|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | flushed
| 0x50	| addi a0, -1		|0x2   |0x204 |0x204 |0x2   |0xc2  |0x204 | 0x61 |0x208 | flushed
| 0x24	| lw a4, a5, 0 		|0x2   |0x204 |0x204 |0x2   |0x20  |0x204 | 0x61 |0x208 | copy value pointed by a5 in a4
| 0x28	| lw a6, a1, 0 		|0x2   |0x204 |0x204 |0x2   |0x20  |0x204 | 0x20 |0x208 | copy value pointed by a1 in a6
| 0x2c	| add a4, a4, a6 	|0x2   |0x204 |0x204 |0x2   |0x40  |0x204 | 0x20 |0x208 | DATA HAZARD : stall by waiting a4. add a4 and a6 and store it in a4
| 0x30	| sw a4, a2	    	|0x2   |0x204 |0x204 |0x2   |0x40  |0x204 | 0x20 |0x208 | DATA HAZARD : a4 is forwarded. store a4 in pointed value of a2 (mem[0x204]=0x40)
| 0x34	| addi a5, a5, 4	|0x2   |0x204 |0x204 |0x2   |0x40  |0x208 | 0x20 |0x208 | add 4 to a5
| 0x38	| addi a1, a1, 4	|0x2   |0x208 |0x204 |0x2   |0xc2  |0x208 | 0x61 |0x208 | add 4 to a1
| 0x3c	| addi a2, a2, 4	|0x2   |0x208 |0x208 |0x2   |0xc2  |0x208 | 0x61 |0x208 | add 4 to a2
| 0x40	| bne a5, a7, -28	|0x2   |0x208 |0x208 |0x2   |0xc2  |0x208 | 0x61 |0x208 | a5 equal to a7 so continue
| 0x44	| jarl ra			|0x2   |0x208 |0x208 |0x2   |0xc2  |0x208 | 0x61 |0x208 | retrun adress (ret)

### 3.2 Pipeline

Now that we have unrolled the whole program and we are sure of each step, we will be able to draw a diagram that will make us understand how the pipeline allows us to accelerate the execution of the program. Thanks to this diagram, we will also be able to easily highlight the hazards that we encounter during the execution.

You will find below the pipline of the instructions : 

![alt text](pipeline.png "Pipeline")

The instructions are RISC-V instructions. They are executed with a 5 steps pipeline (IF/ID/EXEC/MEM/WB). We have highlighted the Hazard sorted by type: 
- Data Hazard which are solved either by forwarding or by stalling (visible in blue and yellow respectively)
- Control Hazard which are solved by flushing (visible in red)

This scheme of execution of all the commands following each step of the pipeline was not easy to realize but allows now to understand easily how the pipeline allows to accelerate the execution of the commands by the processor.


##  4.  Processor Design

### Register set (16 registers of 32 bits)

| 5-bit Encoding (rx) |	Register |ABI Name |	Description
|	---                 |---	     |	---		 |	---	
|0	                  |x0	       |zero	   |hardwired zero
|1	                  |x1	       |ra	     |return address
|2	                  |x2	       |sp	     |stack pointer
|3	                  |x3	       |gp	     |global pointer
|4	                  |x4	       |tp	     |thread pointer
|5	                  |x5	       |t0	     |temporary register 0
|6	                  |x6	       |t1	     |temporary register 1
|7	                  |x7	       |t2	     |temporary register 2
|8	                  |x8	       |s0 / fp	 |saved register 0 / frame pointer
|9	                  |x9	       |s1	     |saved register 1
|10	                  |x10	     |a0	     |function argument 0 / return value 0
|11	                  |x11	     |a1	     |function argument 1 / return value 1
|12	                  |x12	     |a2	     |function argument 2
|13	                  |x13	     |a3	     |function argument 3
|14	                  |x14	     |a4	     |function argument 4
|15	                  |x15	     |pc	     |program counter

Registers a0 to a4 are used as arguments on function call.
Registers a0 and a1 contain the return values of the function.
The callee-save registers are x8 and x9.

### Instructions formats

On the table below, we define the differents instuctions format of our instruction set. We choosed to implement 5 formats, in order to meet all the requirements. Note that all the immediat are sign extended.

| Format | 15 ... 12|11 ... 8 |7 ... 4|   3   |   2    	| 1 ... 0
|	---  |--- 		| ---     |	---	  |	---	  |	---		| ---
| A      | rs2 		| rs1 		| rd 		| f2 		| f1 	| opcode
| B      | imm[4: 1]| rs1 		| rd 		| imm[0] 	| f1 	| opcode
| C      | rs2 		| rs1 		| imm[4: 1]	| imm[0] 	| f1 	| opcode
| D      | imm[8: 5]| imm[4: 1] | rd 		| imm[0] 	| f1 	| opcode
| E      | imm[9: 6]| rs1 		| imm[5: 2] | imm[1] 	| imm[0]| opcode

### Our instruction set : 

| Instruction 	| Format 	| imm size 	| f2 	| f1 	| opcode 	| Expression 					| Explanation							
| --- 			| --- 		| --- 		|--- 	|--- 	| --- 		| --- 							| -- 									
| ADD 			| A 		|			| 0		| 0 	| 00 		| rd = rs1 + rs2 				| add rs1 and rs2, store in rd
| SUB 			| A 		|			| 0		| 1 	| 00   		| rd = rs1 - rs2 				| sub rs2 to rs1, store in rd
| SLL 			| A 		| 			| 1 	| 0 	| 00 		| rd = rs1 << rs2 				| shift rs1 to left of rs2 bits (signed), store in rd
| JMP 			| A 		| 			| 1 	| 1 	| 00 		| PC = rs1              		| jump to the adress stored in rs1
| LW 			| B 		| 5			|		| 0 	| 01 		| rd = MEM[rs1 + imm]			| load value at rs1 + imm in rd
| SW 			| C 		| 5			|		| 1 	| 01 		| MEM[rs2 + imm] = rs1 			| store rs1 at rs2 + imm
| CALL 			| D 	  	| 9			|	    | 0 	| 10 		| ra = PC; PC = imm        		| save PC to ra and jump to imm
| LI 			| D 		| 9			|		| 1		| 10 		| rd = imm                   	| copy imm in rd
| BNZ 			| E 		| 10		|		| 	 	| 11 		| if(rs1) PC = PC + imm * 2 	| jump to pc + imm*2 if rs1 != 0

**Exemples :**

reminder : a0 -> 1010, sp -> 0010
For each instruction we use the table above to get the binary instruction.

| Instruction 	| Assembly code 		| Pseudo-code 			| Binary instruction
| --- 			| --- 					| ---					| ---
| ADD 			| ADD a0, a1, a2 		| a0 = a1 + a2			| 1100 1011 1010 0000
| SUB 			| SUB sp, sp, a4 		| sp -= a4 				| 1101 0010 0010 0100
| SLL 			| SL a2, a3, a4 		| a2 = a3 << a4			| 1101 1100 1011 1000
| JMP 			| JMP ra 				| ret 					| 0000 0000 0001 1100
| LW 			| LW a0, 4(a1) 			| a0 = mem[a1 + 4] 		| 0010 1011 1010 0001
| SW 			| SW a0, a2 			| mem[a2] = a0 			| 1100 1010 0000 0101
| CALL 			| CALL 0x10 			| ra = PC; PC = 0x10 	| 0000 1000 0000 0010
| LI 			| LI t0, 0x10 			| t0 = 0x10				| 0000 1000 0000 0110
| BNZ 			| BNZ a3, 0x11 			| if(a3) PC += 0x22 	| 0000 1101 1000 1011

### The nop instruction

To do the nop instruction, we can do this instruction : add x0, x0, x0.

### Sum function

```asm
0x00	JMP ra
0x02	ADD a0, a0, a1
```

Call of the sum function.
134 = 0b10000110 can be an immediate as it is on 8 bits and LI has a 9 bits immediate operand.
65408 = 0b1111111110000000 cannot be an immediate as it is on 32 bits.

```asm
0x10	LI a0, 134
0x12	CALL 0xFF000000
0x14 	LW a1, 12(pc)
0x16	SW a1, 14(pc)
...
0x20	0xFF80 // 65408
```

An alternative using only immediate (no load word instruction) could have been :
```asm
0x10	LI a0, 134
0x12	LI a1, 0x1ff
0x12	LI a2, 7
0x12	CALL 0x0
0x14 	SLL a1, a1, a2 		// 65408 = 0x1ff << 7
0x16	SW a1, 14(pc)
```

### The original function

Now lets code our initial function with our new set of instructions.
The parameters are still a0, a1, a2, a3 as described in the register set.

```c
if(!a || !b || !c)
	return -1
```
If one of the register a0, a1, a2 is null, we JMP to t0 in which we stored the adress of the `return -1`.
We profit off the branch delay slot to store the return value -1 in a0 after the JMP ra instruction.
```asm
0x0e	LI t0, 0x1a
0x10	BNZ a0, 2
0x12	JMP t0
0x14	BNZ a1, 2
0x16	JMP t0
0x18	BNZ a2, 3
0x1a	JMP ra
0x1c	LI a0, -1
```
```c
if(n <= 0)
	return n;
```
We want to test if a3 <= 0 or we have no instruction to do this.
So we will test the sign bit of a3 by shifting it of 31 bits to the right.
Then we will test if a0 != 0.
We make use of the branch delay slots to begin the computation of `uint32_t* end = a + n;`
```
0x1e	LI t0, 1
0x20	LI t1, -31
0x22	SLL t0, a3, t1
0x24	BNZ t0, 15
0x26	LI t0, 0x2
0x28	BNZ a3, 3
0x2a	SLL a4, a3, t0
0x2c	JMP 0x48
```

Then we finish the computation of `uint32_t* end = a + n;` and we store 4 in a5 to increment our pointers in the loop.
```asm
0x2e	ADD a4, a4, a0
0x30	LI a5, 0x4
```
```c
*c = *a + *b;
```
We load the value read from memory in temporary registers we add them and store t0 back in the memory.
```asm
0x32	LW t0, a0, 0x0
0x34	LW t1, a1, 0x0
0x36	ADD t0, t0, t1
0x38	SW t0, a2, 0x0
```
```c
a++
b++;
c++;
```
We increments our pointers of 4 as the word read are 4 bytes length.
```asm
0x3a	ADD a0, a0, a5
0x3c	ADD a1, a1, a5
0x3e	ADD a2, a2, a5
```
```c
while(a != end)
```
We compare a0 and a4 by substracting them and comparing the result to zero.
We could move our while condition at the end of the loop because at the beginning of loop `a3 != 0` so `a4 != a0`.
```asm
0x40	SUB t0, a4, a0
0x42	BNZ t0, -8
```
```c
return n;
```
We make use of the branch delay slot to return a3.
```asm
0x44	JMP ra
0x46	ADD a0, a3, x0
```

## 4.2 Pipelining

From our instruction set and the SE201 course, we were able to imagine the graphical representation of our processor. As we presented before, we have only 3 steps for our pipelines. Our diagram is therefore cut by 2 big "flip-flop" which symbolize the pipelines. 

Diagram of our processor’s design :

![alt text](Pipeline.png "Pipeline")

Now that we have a logical representation of our processor that should be able to execute our brand new instruction set, we can test it.
To do this, we will execute the CALL instruction, which is one of the most delicate to execute.

We are now going to present you the execution scheme of the CALL instruction by our processor.

![alt text](PipelineWithCALL.png "Pipeline with call")

On this diagram, we can see the 3 steps of the pipeline execution. These steps are all represented by a different color. In blue what happens during the execution of the IF, in green the ID step and in red the state of the processor at the execution of EX.


### Hazards

#### Data hazards

> Assume that the processor registers are written at the beginning of the EX stage and read at the end of the ID stage.

According to this, if the next instruction needs the result of an operation it will be available at the beginnning of the ID stage. So even if it is a conditionnal branch, or a jump or a call, there will be no data hazard.

Only possibility of a data hasard : LW load an adress from memory and then JMP jumps to that adress.
The jump happens in the ID stage but the adress is not available at this time, so the IF and ID stages need to be stalled.

#### Control hazard

> Conditional branches, unconditional jumps, and calls in your instruction set architecture have a branch delay slot for a single instruction

When the jump or branch happens (in the ID stage), the branch delay is read as it should, in the IF stage. Then the PC changes and the next instruction in the IF stage is the correct one.
Then we have **no control hazard** in our pipeline. And we **never need to flush our pipeline**.

#### Structural hazard

A structural hazard happens when two instructions access the same ressource at the same time. The only ressource accessed is the memory and it is only accessed in the EX stage.
So have **no structural hazard** in our pipeline.

</div>